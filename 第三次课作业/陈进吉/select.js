function getDiv()
{
	var div1 = document.getElementById('div1');
	console.log(div1);
	var div2 = document.getElementsByClassName('classdiv');
	console.log(div2);
	var div3 = document.getElementsByTagName('div');
	console.log(div3);
	var div4 = document.getElementsByTagName('p');
	console.log(div4);
	var div5 = document.getElementsByTagName('input');
	console.log(div5);
	var div6 = document.getElementsByTagName('li');
	console.log(div6);
}
function getName()
{
	var name1 = document.getElementsByName('sex');
	console.log(name1);
	var name2 = document.getElementsByName('username');
	console.log(name2);
	var name3 = document.getElementsByName('password');
	console.log(name3);
	var name4 = document.getElementsByName('habbit');
	console.log(name4);
	var name5 = document.getElementsByTagName('input');
	console.log(name5);
	var name6 = document.getElementsByTagName('option');
	console.log(name6);
	var name7 = document.getElementsByTagName('textarea');
	console.log(name7);
}
function getParentElement()
{
	var divLayer4 = document.getElementById('divLayer4');
	console.log(divLayer4.parentElement);
	console.log(divLayer4.parentElement.parentElement);
	console.log(divLayer4.parentElement.parentElement.parentElement);
	console.log(divLayer4.parentElement.parentElement.parentElement.parentElement);
	console.log(divLayer4.parentElement.parentElement.parentElement.parentElement.parentElement);
	console.log(divLayer4.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement);
}
function getParentNode()
{
	var divLayer4 = document.getElementById('divLayer4');
	console.log(divLayer4.parentNode);
	console.log(divLayer4.parentNode.parentNode);
	console.log(divLayer4.parentNode.parentNode.parentNode);
	console.log(divLayer4.parentNode.parentNode.parentNode.parentNode);
	console.log(divLayer4.parentNode.parentNode.parentNode.parentNode.parentNode);
	console.log(divLayer4.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode);	
}
function getDivE_N ()
{
	var div1 = document.getElementById('divLayer1');
	var div2 = document.getElementById('divLayer2');
	var div3 = document.getElementById('divLayer3');
	var div4 = document.getElementById('divLayer4');
	
	console.log(div1.children);
	console.log(div2.children);
	console.log(div3.children);
	console.log(div4.children);
	console.log(' ');
	console.log(div1.childNodes);
	console.log(div2.childNodes);
	console.log(div3.childNodes);
	console.log(div4.childNodes);
}